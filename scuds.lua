local groupbuilder = require("groupbuilder.groupbuilder")
local unitbuilder = require("groupbuilder.unitbuilder")

local ScudUnit = function()
    return unitbuilder.New("Scud_B"):withSkill("Excellent")
end
local ScudGroup = function()
    return groupbuilder.New("Scud-B")
end

local FireAtTask = function(pt)
    return {
        id = 'FireAtPoint',
        params = {
            x = pt.x,
            y = pt.y,
            point = pt,
            radius = 300,
            expendQty = 4,
            expendQtyEnabled = true
        }
    }
end

Scuds = {}

Scuds.MinimumRange = 70000

Scuds.OnFireHandler = function(event)
    --Check to see if we care...
    if event.id ~= world.event.S_EVENT_SHOT then return end
    local weapon = event.weapon
    if not weapon then log("SCUDS - Shot event had no weapon!"); return end
    local weaponName = weapon:getDesc()["displayName"]
    if weaponName ~= "Scud R-17" then return end
    -- Congrats. It's a scud!
    -- We only care about the first fire right now.
    if game_stats.scuds.fired then return end

    log("SCUDS - Detected initial fire from SCUD group")

    -- Ok. Scud launched and its the first of its group.
    MessageToAll("SCUD Alert! We have detected SCUD fire!\n" ..
                 "The target is considered to be "..game_stats.scuds.target:getName())
    game_stats.scuds.fired = true
    game_stats.scuds.active = false
end
mist.addEventHandler(Scuds.OnFireHandler)

-- Orders the [[scudGroup]] group to shoot at [[position]] with [[count]] rounds.
-- It will attempt to get a controller, so if the group or the controller don't
-- respond then we won't shoot.
Scuds.FireAt = function(scudGroup, position, count)
    local group = Group.getByName(scudGroup.name)
    if group then
        local ctrl = group:getController()
        if ctrl then
            position = mist.utils.makeVec2(position)
            ctrl:pushTask(FireAtTask(position))
            mist.scheduleFunction(function()
                local offSet = randomOffset(30, 350)
                position = {
                    x = position.x + offSet.x,
                    y = position.y + offSet.y
                }
                ctrl:pushTask(FireAtTask(position))
            end, {}, timer.getTime() + 540, 60, timer.getTime() + 540 + 60*(count-1))
        else
            log("SCUDS - Scuds were going to fire but we couldn't get a handle on the groups's controller. The group may be dead.")
            Scuds.Cleanup()
        end
    else
        log("SCUDS - Scuds were going to fire but we couldn't get a handle on the group. It might be dead.")
        Scuds.Cleanup()
    end
end

-- Cleans up the game_stats so that the next group can spawn.
Scuds.Cleanup = function()
    game_stats.scuds.active = false
    game_stats.scuds.fired = false
end

-- This is triggered when the mission wants to first make the players aware
-- Of the scuds. We check if the scud group is still alive first though.
Scuds.ScudsFoundAlert = function(scudGroup)
    scudGroup = Group.getByName(scudGroup.name)
    if not isAlive(scudGroup) then
        log("SCUDS - We were about to alert on the SCUD group, but it's not alive anymore!")
        game_stats.scuds = {}
        return
    end

    local originAirfieldName = game_stats.scuds.origin:getName()
    local scudGroupSize= scudGroup:getSize()
    local alert = "ATTENTION\n"..
                  "===================================================\n"..
                  "SCUD LAUNCHERS HAVE BEEN SIGHTED NEAR " .. originAirfieldName ..".\n"..
                  scudGroupSize .." LAUNCHERS APPEAR READY TO FIRE.\n"..
                  "THE TARGET IS CURRENTLY UNKNOWN\n"
    MessageToAll(alert)

end
Scuds.SpawnGroupAt = function(quantity, position)
    local units = {}
    for i=1,quantity do
        local offset = randomOffset(200, 450)
        table.insert(units, ScudUnit():withOffset(offset))
    end
    local group = ScudGroup():withCountry("Russia"):withUnits(units):atPosition(position)
    local spawn = group:spawn()
    spawn.FireAt = function(self,position,count) Scuds.FireAt(self,position,count) end
    return spawn
end


return Scuds